import './App.css';

function App() {
  return (
    <div className="body">
      <header>
        <h1 contenteditable>Website.com</h1>
      </header>

      <div className="left-sidebar" contenteditable>
        <ul>
          <li>home</li>
          <li>about</li>
          <li>profile</li>
        </ul>
      </div>

      <main contenteditable>
        <ul>
          <li>userId: 1</li>
          <li>id: 1</li>
          <li>title: sunt aut facere repellat provident occaecati excepturi optio reprehenderit</li>
          <li>body: quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto</li>
        </ul>

      </main>

      <div className="right-sidebar" contenteditable>
        <p>lorem ipsum</p>
      </div>

      <footer contenteditable>
        Footer Content - Website.com 2020
      </footer>
    </div>
  );
}

export default App;
